package com.example.ejerciciopersistenciaa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre, etDatos;
    private Button btnGuardar, btnBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializarVistas();
    }

    private void inicializarVistas() {
        etNombre = findViewById(R.id.etNombre);
        etDatos = findViewById(R.id.etDatos);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnBuscar = findViewById(R.id.btnBuscar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarDatos();
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarDatos();
            }
        });
    }

    private void buscarDatos() {
      String nombre = etNombre.getText().toString();
      String datos;
      if (nombre.length() != 0) {
          SharedPreferences preferencias = getSharedPreferences(
                  "agenda",
                  Context.MODE_PRIVATE
          );
          datos = preferencias.getString(nombre,"");
          if(datos.length() != 0) {
              etDatos.setText(datos);
          } else {
              Toast.makeText(this, "No se encontro el registro", Toast.LENGTH_SHORT).show();
          }
      } else {
          Toast.makeText(this, "Debes poner un nombre", Toast.LENGTH_SHORT).show();
      }
    }

    private void guardarDatos() {
        String nombre = etNombre.getText().toString();
        String datos = etDatos.getText().toString();
        if (nombre.length() != 0) {
            SharedPreferences preferencias = getSharedPreferences(
                    "agenda",
                    Context.MODE_PRIVATE
            );
            SharedPreferences.Editor editor = preferencias.edit();
//            editor.putString("nombre", nombre);
//            editor.putString("datos", datos);
            /*
            imaginense que la figura estuviera de esta forma:
                "jose","av. siempre viva 123",
                "sergio", "calle de los lamentos",
                "adamary", "av. uno",
                "jose", "dadsadsada" X
             */
            editor.putString(nombre, datos);
            /*
            esta instruccion espera a que todos los registros
            se hayan actualizado para recien seguir con las siguientes
            lineas de codigo......
             */
            editor.commit();
            /*
            segunda manera
            continua con las siguientes lineas de codigo, mientras
            en segundo plano se esta actualizando el archivo....
             */
            //editor.apply();
            Toast.makeText(this, "El contacto ha sido creado", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Debes poner un nombre", Toast.LENGTH_SHORT).show();
        }


    }
}
